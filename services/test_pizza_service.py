from unittest import TestCase, main
from mongoengine import connect, disconnect
from models import IPizza as PizzaModel
from services import (
    add_many_pizzas,
    add_pizza,
    read_all_pizzas,
    read_pizza,
    modify_pizza,
    delete_all_pizzas,
    delete_pizza
)


class TestPizzaService(TestCase):

    @classmethod
    def setUpClass(cls):
        connect('aunip', host='mongomock://localhost')

    @classmethod
    def tearDownClass(cls):
        disconnect()

    def tearDown(self):
        # Clear
        for p in PizzaModel.objects():
            p.delete()

    def test_add_many_pizzas(self):
        all_pizzas = []

        for i in range(0, 10):
            items = []

            for j in range(1, i + 1):
                items.append('Item ' + str(j))

            all_pizzas.append({'label': 'Pizza ' + str(i), 'items': items, 'price': 9.99 + i})

        result = add_many_pizzas(all_pizzas)

        self.assertEqual(result['createdCount'], len(all_pizzas))

    def test_add_pizza(self):
        new_pizza = {'label': 'PizzaTella', 'items': ['Choco', 'Nuts'], 'price': 9.99}

        result = add_pizza(new_pizza)

        pizza = PizzaModel.objects(label='PizzaTella').get()

        self.assertEqual(result['createdId'], str(pizza.id))

    def test_read_all_pizzas(self):
        for i in range(0, 10):
            items = []

            for j in range(1, i + 1):
                items.append('Item ' + str(j))

            new_pizza = PizzaModel(label='Pizza ' + str(i), items=items, price=9.99 + i)
            new_pizza.save()

        results = read_all_pizzas()

        self.assertEqual(len(results), 10)

    def test_read_pizza(self):
        new_pizza = PizzaModel(label='PizzaTella', items=['Choco', 'Nuts'], price=9.99)
        new_pizza.save()

        pizza = PizzaModel.objects(label='PizzaTella').get()

        result = read_pizza(str(pizza.id))

        self.assertEqual(str(result.id), str(pizza.id))
        self.assertEqual(result.label, 'PizzaTella')
        self.assertEqual(len(result.items), 2)
        self.assertEqual(result.price, 9.99)

    def test_modify_pizza(self):
        new_pizza = PizzaModel(label='PizzaTella', items=['Choco', 'Nuts'], price=9.99)
        new_pizza.save()

        pizza = PizzaModel.objects(label='PizzaTella').get()

        result = modify_pizza(str(pizza.id), {'label': 'Pizza', 'items': ['Nutella'], 'price': 10})

        self.assertEqual(result['updatedId'], str(pizza.id))

        modified_pizza = PizzaModel.objects(label='Pizza').get()

        self.assertEqual(modified_pizza.label, 'Pizza')
        self.assertEqual(len(modified_pizza.items), 1)
        self.assertEqual(modified_pizza.price, 10)

    def test_delete_all_pizzas(self):
        for i in range(0, 10):
            items = []

            for j in range(1, i + 1):
                items.append('Item ' + str(j))

            new_pizza = PizzaModel(label='Pizza ' + str(i), items=items, price=9.99 + i)
            new_pizza.save()

        result = delete_all_pizzas()

        self.assertEqual(result['deletedCount'], 10)

    def test_delete_pizza(self):
        new_pizza = PizzaModel(label='PizzaTella', items=['Choco', 'Nuts'], price=9.99)
        new_pizza.save()

        pizza = PizzaModel.objects(label='PizzaTella').get()

        result = delete_pizza(str(pizza.id))

        self.assertEqual(result['deletedId'], str(pizza.id))


if __name__ == '__main__':
    main()
