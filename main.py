from mongoengine import connect
from flask import Flask
from flask_graphql import GraphQLView
from services import init_pizzas
from schema import schema

connect('aunip', host='localhost', port=27017)

app = Flask(__name__)

app.add_url_rule('/graphql', view_func=GraphQLView.as_view('graphql', schema=schema, graphiql=True))

PORT = 5050


if __name__ == '__main__':
    init_pizzas()
    print("> Listening On 'http://localhost:" + str(PORT) + "'")
    app.run(host='localhost', port=PORT)
