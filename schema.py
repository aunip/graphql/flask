from graphene import ObjectType, Field, List, String, Float, Schema, Mutation
from graphene_mongo import MongoengineObjectType
from models import IPizza as PizzaModel
from services import (
    read_all_pizzas,
    read_pizza,
    add_pizza,
    modify_pizza,
    delete_pizza
)


class Pizza(MongoengineObjectType):
    class Meta:
        model = PizzaModel


class Query(ObjectType):
    pizzas = List(Pizza)
    pizza = Field(Pizza, id=String())

    @staticmethod
    def resolve_pizzas(self, *args):
        return read_all_pizzas()

    @staticmethod
    def resolve_pizza(self, *args, id):
        return read_pizza(id)


class CreatePizza(Mutation):
    class Arguments:
        label = String()
        items = List(String)
        price = Float()

    createdId = String()

    @staticmethod
    def mutate(self, *args, **kwargs):
        result = add_pizza(kwargs)

        if not result:
            return CreatePizza(createdId=None)

        return CreatePizza(createdId=result['createdId'])


class UpdatePizza(Mutation):
    class Arguments:
        id = String()
        label = String()
        items = List(String)
        price = Float()

    updatedId = String()

    @staticmethod
    def mutate(self, *args, id, **kwargs):
        result = modify_pizza(id, kwargs)

        if not result:
            return UpdatePizza(updatedId=None)

        return UpdatePizza(updatedId=result['updatedId'])


class DeletePizza(Mutation):
    class Arguments:
        id = String()

    deletedId = String()

    @staticmethod
    def mutate(self, *args, id):
        result = delete_pizza(id)

        if not result:
            return DeletePizza(deletedId=None)

        return DeletePizza(deletedId=result['deletedId'])


class Mutation(ObjectType):
    create_pizza = CreatePizza.Field()
    update_pizza = UpdatePizza.Field()
    delete_pizza = DeletePizza.Field()


schema = Schema(query=Query, mutation=Mutation)
