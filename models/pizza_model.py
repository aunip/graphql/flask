from mongoengine import Document
from mongoengine.fields import ObjectIdField, StringField, ListField, FloatField
from bson import ObjectId


class IPizza(Document):
    meta = {'collection': 'pizzas'}
    id = ObjectIdField(name='_id', default=ObjectId, primary_key=True)
    label = StringField()
    items = ListField(StringField())
    price = FloatField()
