from unittest import TestCase, main
from mongoengine import connect, disconnect
from models import IPizza as PizzaModel


class TestPizzaModel(TestCase):

    @classmethod
    def setUpClass(cls):
        connect('aunip', host='mongomock://localhost')

    @classmethod
    def tearDownClass(cls):
        disconnect()

    def tearDown(self):
        # Clear
        for p in PizzaModel.objects():
            p.delete()

    def test_pizza_model(self):
        new_pizza = PizzaModel(label='PizzaTella', items=['Choco', 'Nuts'], price=9.99)
        new_pizza.save()

        pizza = PizzaModel.objects().get()

        self.assertTrue(str(pizza.id))
        self.assertEqual(pizza.label, 'PizzaTella')
        self.assertEqual(len(pizza.items), 2)
        self.assertEqual(pizza.price, 9.99)


if __name__ == '__main__':
    main()
