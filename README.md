# All U Need Is Pizza

> _Made With **Python** 3_

Isomorphic Projects For Testing Technologies With CRUD Pattern

## File Structure

```
.
+-- models
    +-- __init.py__
    +-- pizza_model.py
    +-- test_pizza_model.py
+-- services
    +-- __init.py__
    +-- pizza_service.py
    +-- test_pizza_service.py
+-- .gitignore
+-- LICENSE
+-- main.py
+-- pizzas.json
+-- README.md
+-- requirements.txt
+-- schema.py
```

## Process

Repository:

```
git clone https://gitlab.com/aunip/graphql/python.git
```

Init:

```
python -m venv env
source env/bin/activate
```

Install:

```
pip install -r requirements.txt
```

Launch:

```
python main.py
```

Test:

```
python -m unittest
```

### Requirement

- [x] **MongoDB** Server

## License

```
"THE BEER-WARE LICENSE" (Revision 42):
<phk@FreeBSD.ORG> wrote this file. As long as you retain this notice you
can do whatever you want with this stuff. If we meet some day, and you think
this stuff is worth it, you can buy me a beer in return. Damien Chazoule
```
